#include <algorithm>
#include <random>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <algorithm>
#include <array>
#include <cassert>
#include <tuple>
#include <set>

using namespace std;

#define TEST

const int NO_PLACE = -1;

const bool debug = false;

struct Point
{
  int y;
  int x;

  bool operator==(const Point &p) const;
};

bool Point::operator==(const Point &p) const
{
  return y == p.y && x == p.x;
}

struct TroopsInfo
{
  int playerId;
  int fromBase;
  int toBase;
  int size;
  int y;
  int x;
  double distance;
  int startTime;
  int arrivedTime;
  
  void updateCoords(int time, int speed, int sy, int ey, int sx, int ex);
  
  bool arrived(int time, int speed);
  bool operator==(const TroopsInfo &info) const;
  void setStartTime(const int time, const int speed);
  void setEndTime(const int curTime, const int endTime, const int speed);
};

// distanceが正しく設定されている前提
void TroopsInfo::setStartTime(const int time, const int speed)
{
  startTime = time;
  arrivedTime = startTime + (int)(ceil(distance / speed));
}

// distanceが正しく設定されている前提
void TroopsInfo::setEndTime(const int curTime, const int endTime, const int speed)
{
  const int diff = (int)(ceil(distance / speed));
  startTime = max(curTime, arrivedTime - diff);
  if(startTime == curTime)
  {
	arrivedTime = startTime + diff;
  }
  else
  {
	arrivedTime = endTime;
  }
}

ostream &operator<<(ostream &out, const TroopsInfo &src)
{
  out << " playerId: " << src.playerId;
  out << " size: " << src.size;
  out << " from: " << src.fromBase;
  out << " to: " << src.toBase;
  out << " dist: " << src.distance;
  out << " y: " << src.y;
  out << " x: " << src.x;
  out << " start: " << src.startTime;
  out << " arrived: " << src.arrivedTime;
  return out;
}

bool TroopsInfo::operator==(const TroopsInfo &info) const
{
  return (size == info.size) &&
	(playerId == info.playerId) &&
	(y == info.y) &&
	(x == info.x);
}

void TroopsInfo::updateCoords(int time, int speed, int sy, int ey, int sx, int ex)
{
  const double partMoved = (time - startTime) * 1.0 / (arrivedTime - startTime);
  const double dy = ey - sy;
  const double dx = ex - sx;
  y = (int)(sy + dy * partMoved);
  x = (int)(sx + dx * partMoved);
}

bool TroopsInfo::arrived(int time, int speed)
{
  return time >= arrivedTime;
}

struct Base
{
  const int y;
  const int x;
  int growthRate;
  int playerId;
  int troopsSize;
  
  // for strategy
  vector<pair<int, int>> nearestSelfBaseIndices;
  int strategyId;
  int turnToAct;
  int targetBaseId;
  
  // 目的地などが既にわかっているtroopsリスト
  vector<TroopsInfo> selfTroops;
  vector<TroopsInfo> opponentTroops;
  
  Base(int y, int x);
};

ostream &operator<<(ostream &out, const Base &src)
{
  out << "(y, x) = (" << src.y << ", " << src.x << ")";
  out << " gr = " << src.growthRate;
  out << " playerId: " << src.playerId;
  out << " size: " << src.troopsSize;
  out << " strategy: " << src.strategyId;
  out << " turnToAct: " << src.turnToAct;
  out << " targetBaseId: " << src.targetBaseId;
  out << endl;

  out << "self troops: " << endl;
  for(auto &t : src.selfTroops)
  {
	out << t << endl;
  }
  out << "opponent troops" << endl;
  for(auto &t : src.opponentTroops)
  {
	out << t << endl;
  }  
  
  return out;
}

Base::Base(int y, int x)
  : y(y)
  , x(x)
  , strategyId(-2)
  , turnToAct(-1)
  , targetBaseId(-1){}

struct Player
{
  double strengthUB;
  double strengthLB;
  int attackUB;
  int attackLB;
  int troopsNum;

  Player()
	: strengthUB(1.2)
	, strengthLB(1.0)
	, attackUB(1000)
	, attackLB(500){}
};

struct BaseInfo
{
  int baseId;
  int size;
  int playerId;
  int startTime;
  int prevY;
  int prevX;

  bool operator==(const BaseInfo &info) const;
};

ostream &operator<<(ostream &out, const BaseInfo &src)
{
  out << " baseId: " << src.baseId;
  out << " size: " << src.size;
  out << " playerId: " << src.playerId;
  out << " start: " << src.startTime;
  return out;
}


bool BaseInfo::operator==(const BaseInfo &info) const
{
  return (baseId == info.baseId) &&
	(size == info.size) &&
	(playerId == info.playerId) &&
	(startTime == info.startTime);
}  

array<array<array<int, 1001>, 1001>, 4> table;
// [gr][current] から始めてtroopsが1000に到達しない最大の時間
array<array<int, 1001>, 4> table1000;

class AbstractWars {
public:
  int init(vector <int> baseLocations, int speed);
  vector<int> sendTroops(const vector <int> &bases, const vector <int> &troops);
  
private:
  const int FIRST_TURN = 0;

  const int KILL_MOVE_RESERVED = 2;
  const int REVERT_MOVE_RESERVED = 1;
  const int DEFENSE_MOVE_RESERVED = 0;
  const int NEAREST_MOVE_RESERVED = -1;
  const int NOT_RESERVED = -2;
  
  int B;
  int speed;
  int turn;
  // 各頂点で算出した，「隣接する点の最小距離」の最大値
  int UBTime;
  int UBDist2;

  vector<int> otherBaseId;
  vector<int> selfBaseId;
  vector<int> allBaseId;

  vector<Base> baseList;
  vector<Player> playerList;

  vector<BaseInfo> nondecidedBaseInfo;
  array<int, 100> baseSize_tmp;
  
  mt19937 mt;
  uniform_real_distribution<double> rand;

  int strongestPlayerId;
  
  int getNearestOpponentBase(const int sourceInd);
  pair<int, int> getNearSelfBase(const int sourceInd, const int LB, const int UB, const vector<int> &candidates);
  int getCornerOpponentBase(const int sourceInd);
  
  int distance2(int base1, int base2);
  double distance(int base1, int base2);

  void initializeInTurn(const vector<int> &bases, const vector<int> &troops);
  void generateAttr(vector<int> &att);
  void decideTroops();
  void killMove();
  void runAwayMove();
  void nearestMove();
  void defenseMove();
  void revertMove();
  
  void initializeTable();
  void initializeBase(const vector<BaseInfo> &bases);
  void estimateTroops(vector<TroopsInfo> &troops);
  void collectTroopsNum(vector<TroopsInfo> &troops, vector<BaseInfo> &bases);
  void removeArrivedTroops();
  void removeEstimatedTroops(vector<TroopsInfo> &troops);
  void decideNondeterminedBase(vector<TroopsInfo> &troops);
  void matchingFirstTurnTroops(vector<TroopsInfo> &troops);

  void gatherOpponentTimePrev(const int base, vector<int> &vs,
							  vector<TroopsInfo> &oppoTroops);
  void gatherOpponentTimeNext(const int base, vector<int> &vs,
							  vector<TroopsInfo> &oppoTroops);
	
  bool isValid1(const Base &base, const TroopsInfo &troop) const;
  bool isValid(const BaseInfo &base, const TroopsInfo &troop) const;
  bool isValid2(const BaseInfo &baseInfo,
			   const TroopsInfo &troopsInfo,
			   const Base &base) const;
  
  int dist2ToTime(const int dist2) const;
  int timeToDist2(const int time) const;
  double distanceWithDiff(double dy, double dx, double diff) const;

  // 到着時刻を最適化
  tuple<int, int, int> optimizeArrivedTurn(const int baseId,
										   vector<TroopsInfo> &selfTroops,
										   vector<TroopsInfo> &oppoTroops,
										   const vector<int> &vs,
										   const vector<int> &additionalSelfBase);

  const int NONE = -1;
  int matchBaseTroop(const BaseInfo &base, const TroopsInfo &troop) const;

  // 現状のtroopsで，全ての戦闘が終了した時に，残っていたAIをplayerの人数ベースで算出
  // (playerId, troops)を返す
  pair<int, int> simulate(const int baseIndex,
			   const vector<TroopsInfo> &opponents,
			   const vector<TroopsInfo> &self) const;
  pair<int, int> simulate(const int baseIndex,
						  const vector<TroopsInfo> &opponents,
						  const vector<TroopsInfo> &self,
						  const int timeUB) const;
  
  pair<int, int> simulate(const int baseIndex) const;
  pair<int, int> simulate(const int baseIndex, const int timeUB) const;
	
  TroopsInfo createTroops(const int fromBase, const int toBase);
};

TroopsInfo AbstractWars::createTroops(const int fromBase, const int toBase) 
{
  TroopsInfo info;
  info.playerId = 0; // selfであることに注意
  info.fromBase = fromBase;
  info.toBase = toBase;
  info.size = baseList[fromBase].troopsSize / 2;
  info.y = baseList[info.fromBase].y;
  info.x = baseList[info.fromBase].x;
  info.distance = distance(info.fromBase, info.toBase);
  info.startTime = turn;
  info.arrivedTime = turn + (int)(ceil(info.distance / speed));

  return info;
}

const int INF = 100000;

pair<int, int> AbstractWars::simulate(const int baseIndex) const
{
  return simulate(baseIndex, baseList[baseIndex].opponentTroops, baseList[baseIndex].selfTroops, INF);
}

pair<int, int> AbstractWars::simulate(const int baseIndex, const int timeUB) const
{
  return simulate(baseIndex, baseList[baseIndex].opponentTroops, baseList[baseIndex].selfTroops, timeUB);
}

pair<int, int> AbstractWars::simulate(const int baseIndex,
									  const vector<TroopsInfo> &opponents,
									  const vector<TroopsInfo> &self) const
{
  return simulate(baseIndex, opponents, self, INF);
}

pair<int, int> AbstractWars::simulate(const int baseIndex,
									  const vector<TroopsInfo> &opponents,
									  const vector<TroopsInfo> &self,
									  const int timeUB) const
{
  vector<TroopsInfo> allTroops;
  for(auto &o : opponents)
  {
	allTroops.push_back(o);
  }
  for(auto &s : self)
  {
	allTroops.push_back(s);
  }
  auto cmp = [](const TroopsInfo &t1, const TroopsInfo &t2){
	return t1.arrivedTime < t2.arrivedTime;
  };
  sort(allTroops.begin(), allTroops.end(), cmp);

  // troopsSize, playerIdを管理するために使う
  Base base(baseList[baseIndex]);
  const int gr = base.growthRate;
  int prev_turn = turn;
  for(const auto &t : allTroops)
  {
	if(t.arrivedTime > timeUB)
	{
	  return make_pair(base.playerId, base.troopsSize);
	}
	const int timeDiff = min(timeUB, t.arrivedTime) - prev_turn;
	base.troopsSize = table[gr][base.troopsSize][timeDiff];

	if(base.playerId == t.playerId)
	{
	  base.troopsSize = min(1000, base.troopsSize + t.size);
	}
	else
	{
	  const double defensePower = base.troopsSize * playerList[base.playerId].strengthUB;
	  const double offensePower = t.size * playerList[t.playerId].strengthUB;
	  if(defensePower < offensePower)
	  {
		base.playerId = t.playerId;
	  }
	  base.troopsSize = abs(offensePower - defensePower) / playerList[base.playerId].strengthUB;
	}
	prev_turn = t.arrivedTime;
  }
  if(timeUB != INF && prev_turn < timeUB)
  {
	const int diff = min(1000, timeUB - prev_turn);
	base.troopsSize = table[gr][base.troopsSize][diff];
  }
  return make_pair(base.playerId, base.troopsSize);
}

void AbstractWars::gatherOpponentTimeNext(const int base, vector<int> &vs,
										  vector<TroopsInfo> &oppoTroops)
{
  vs.clear();

  vs.push_back(turn);
  for(auto t : oppoTroops)
  {
	if(t.arrivedTime > turn)
	{
	  vs.push_back(t.arrivedTime + 1);
	}
  }
  sort(vs.begin(), vs.end());
  vs.erase(unique(vs.begin(), vs.end()), vs.end());
}

void AbstractWars::gatherOpponentTimePrev(const int base, vector<int> &vs,
							vector<TroopsInfo> &oppoTroops)
{
  vs.clear();
  
  vs.push_back(turn);
  for(auto t : oppoTroops)
  {
	if(t.arrivedTime > turn)
	{
	  vs.push_back(t.arrivedTime - 1);
	}
  }
  sort(vs.begin(), vs.end());
  vs.erase(unique(vs.begin(), vs.end()), vs.end());
}

tuple<int, int, int> AbstractWars::optimizeArrivedTurn(const int baseId,
													   vector<TroopsInfo> &selfTroops,
													   vector<TroopsInfo> &oppoTroops,
													   const vector<int> &vs,
													   const vector<int> &additionalSelfBase)
{
  const int evaluateTime = vs.back();
  const int gr = baseList[baseId].growthRate;
  
  int bestPlayerId = -1;  
  int bestSize = -2000;
  int bestEndTime = -1;
  
  for(int endTime : vs)
  {
	int count = 0;
	for(auto newId : additionalSelfBase)
	{
	  if(baseList[newId].strategyId == NOT_RESERVED)
	  {
		auto self = createTroops(newId, baseId);
		
		self.setEndTime(turn, endTime, speed);
		auto v = simulate(newId, self.startTime);
		
		if(v.first == 0)
		{
		  count++;
		  self.size = v.second / 2;
		  selfTroops.push_back(self);
		}
	  }
	}
	int playerId, size;
	tie(playerId, size) = simulate(baseId, oppoTroops, selfTroops);
	const int timeDiff = evaluateTime - endTime;
	size = table[gr][size][timeDiff];
	if (playerId != 0)
	{
	  size = (int)(size * -playerList[playerId].strengthUB);
	}
	if (bestSize < size)
	{
	  bestSize = size;
	  bestEndTime = endTime;
	  bestPlayerId = playerId;
	}
	for(int i = 0; i < count; i++)
	{
	  selfTroops.pop_back();
	}
  }
  return make_tuple(bestPlayerId, bestSize, bestEndTime);
}

double AbstractWars::distanceWithDiff(double dy, double dx, double diff) const
{
  dy = max(0.0, abs(dy) + diff);
  dx = max(0.0, abs(dx) + diff);
  return sqrt(dy * dy + dx * dx);
}

int AbstractWars::dist2ToTime(const int dist2) const
{
  return (int)((ceil(sqrt(dist2)) + speed - 1) / speed);
}

int AbstractWars::timeToDist2(const int time) const
{
  int dist = (time + 1) * speed;
  return dist * dist - 1;
}

void AbstractWars::initializeTable()
{
  for(int growthRate = 1; growthRate <= 3; growthRate++)
  {
	for(int time = 0; time <= 1000; time++)
	{
	  table1000[growthRate][time] = 0;
	}
  }

  
  for(int growthRate = 1; growthRate <= 3; growthRate++)
  {
	for(int time = 0; time <= 1000; time++)
	{
	  table[growthRate][0][time] = 0;
	}
  	for(int troopSize = 1; troopSize <= 1000; troopSize++)
  	{
	  table[growthRate][troopSize][0] = troopSize;
	  for(int time = 1; time <= 1000; time++)
	  {
		table[growthRate][troopSize][time] =
		  min(1000, table[growthRate][troopSize][time - 1] + growthRate + table[growthRate][troopSize][time - 1] / 100);
		
		if(table[growthRate][troopSize][time] < 1000)
		{
		  table1000[growthRate][troopSize] = time;
		}
	  }
  	}
  }
}

int AbstractWars::distance2(int base1, int base2)
{
  const int dx = baseList[base1].x - baseList[base2].x;
  const int dy = baseList[base1].y - baseList[base2].y;
  return dx * dx + dy * dy;
}

double AbstractWars::distance(int base1, int base2)
{
  return sqrt(distance2(base1, base2));
}

int AbstractWars::init(vector <int> baseLocations, int speed)
{
  rand = uniform_real_distribution<double>(0, 1);
  
  B = baseLocations.size() / 2;
  this->speed = speed;
  turn = FIRST_TURN - 1;
  
  for (int i = 0; i < B; ++i) {
	const int y = baseLocations[2*i+1];
	const int x = baseLocations[2*i];
	baseList.push_back(Base(y, x));
	allBaseId.push_back(i);
  }
  initializeTable();

  UBDist2 = 0;
  for(int i = 0; i < baseList.size(); i++)
  {
	int localMin = 10000000;
	for(int j = 0; j < baseList.size(); j++)
	{
	  if(i != j)
	  {
		localMin = min(localMin, distance2(i, j));
	  }
	}
	UBDist2 = max(UBDist2, localMin);
  }
  UBTime = dist2ToTime(UBDist2);

  fill(baseSize_tmp.begin(), baseSize_tmp.end(), 0);
  
  return 0;
}

int AbstractWars::getCornerOpponentBase(const int sourceInd)
{
  const int bestY = baseList[sourceInd].y >= 300 ? 600 : 0;
  const int bestX = baseList[sourceInd].x >= 300 ? 600 : 0;

  int minDist2 = 1000000000;
  int minIndex = -1;
  
  for(int opponentId : otherBaseId)
  {
	int playerId, size;
	tie(playerId, size) = simulate(opponentId);
	if(playerId != 0)
	{
	  const int dy = bestY - baseList[opponentId].y;
	  const int dx = bestX - baseList[opponentId].x;
	  const int dist2 = dy * dy + dx * dx;
	  if(dist2 < minDist2)
	  {
		minDist2 = dist2;
		minIndex = opponentId;
	  }
	}
  }
  return minIndex;
}

pair<int, int> AbstractWars::getNearSelfBase(const int sourceInd, const int LB, const int UB, const vector<int> &candidates)
{
  int selectDist2 = 100000000;
  int selectedBase = -1;
  
  for(auto selfId : candidates)
  {
	if(sourceInd != selfId)
	{
	  const int dist2 = distance2(sourceInd, selfId);
	  int playerId, size;
	  tie(playerId, size) = simulate(selfId);
	  if(playerId != 0)
	  {
		size = (int)(size * -playerList[playerId].strengthUB);
	  }
	  if(LB <= size && size <= UB && dist2 <= selectDist2)
	  {
		selectDist2 = dist2;
		selectedBase = selfId;
	  }
	}
  }
  return make_pair(selectedBase, selectDist2);
}

int AbstractWars::getNearestOpponentBase(const int sourceInd)
{
  int selectedDist2[2] = {100000000, 100000000};
  int selectedBase[2] = {-1, -1};
  
  int minDist2 = 100000000;
  int minBase = -1;
  
  for(auto otherId : otherBaseId)
  {
	const int dist2 = distance2(sourceInd, otherId);
	if(minDist2 > dist2)
	{
	  minDist2 = dist2;
	  minBase = otherId;
	}
	
	int playerId, size;
	tie(playerId, size) = simulate(otherId);
	if(playerId != 0)
	{
	  size = (int)(size * -playerList[playerId].strengthUB);
	}
	const int idx = (playerId == strongestPlayerId) ? 1 : 0;
	if(selectedDist2[idx] > dist2 && size <= 500)
	{
	  selectedDist2[idx] = dist2;
	  selectedBase[idx] = otherId;
	}
  }
  const double rate = 2;

  if(selectedBase[0] == -1 && selectedBase[1] >= 0)
  {
	return selectedBase[1];
  }
  else if(selectedBase[0] >= 0 && selectedBase[1] == -1)
  {
	return selectedBase[0];
  }
  else if(selectedBase[0] == -1 && selectedBase[1] == -1)
  {
	return minBase;
  }
  else
  {
	if(selectedDist2[0] * rate < selectedDist2[1])
	{
	  return selectedBase[0];
	}
	else
	{
	  return selectedBase[1];
	}
  }
}

void AbstractWars::killMove()
{
  if(!selfBaseId.empty())
  {
	// index番目の自分のBaseとの距離が近い順に，敵のBaseIndexをソート
	vector<pair<int, int>> otherBaseIdIndices;
	for(int idOther : otherBaseId)
	{
	  int playerId, size;
	  tie(playerId, size) = simulate(idOther);
	  
	  int index = 0;
	  while(index < baseList[idOther].nearestSelfBaseIndices.size() &&
			baseList[idOther].nearestSelfBaseIndices[index].first < UBDist2 &&
			playerId != 0)
	  {
		index++;
	  }
	  index--;
	  if(index >= 0)
	  {
		const int priority = index;
		otherBaseIdIndices.push_back(make_pair(priority, idOther));
	  }
	}
	sort(otherBaseIdIndices.begin(), otherBaseIdIndices.end(), greater<pair<int, int>>());
	if(debug) cerr << "kill candidate" << endl;
	if(debug) for(auto v : otherBaseIdIndices) cerr << v.second << " ";
	if(debug) cerr << endl;
	
	// 敵が追加の援軍を送っていなかった場合，必ず勝てるなら最小限の兵力を送る．
	for(const auto &v : otherBaseIdIndices)
	{
	  const int idOther = v.second;
	  const int index = v.first;

	  vector<int> times;
	  gatherOpponentTimeNext(idOther, times, baseList[idOther].opponentTroops);
	  
	  vector<int> newIds;
	  for(int j = 0; j <= index; j++)
	  {
		newIds.push_back(baseList[idOther].nearestSelfBaseIndices[j].second);

		if(debug) cerr << "exec " << "oppos; " << idOther << "index " << index << endl;
		int playerId, size, endTurn;
		tie(playerId, size, endTurn) = optimizeArrivedTurn(idOther,
														   baseList[idOther].selfTroops,
														   baseList[idOther].opponentTroops,
														   times,
														   newIds);
		if(debug) cerr << "playerId = " << playerId << " size = " << size << " endTurn = " << endTurn << endl;

		if(playerId == 0 && size >= 2)
		{
		  for(int i = 0; i <= j; i++)
		  {
			int latestTime = 0;
			const int idSelf = newIds[i];
			
			if(baseList[idSelf].strategyId == NOT_RESERVED)
			{
			  baseList[idSelf].targetBaseId = idOther;
			  baseList[idSelf].turnToAct = turn;
			  baseList[idSelf].strategyId = KILL_MOVE_RESERVED;
			  
			  TroopsInfo info = createTroops(baseList[idOther].nearestSelfBaseIndices[i].second, idOther);
			  info.setEndTime(turn, endTurn, speed);
			  auto v = simulate(idSelf, info.startTime);
			  
			  if(v.first == 0)
			  {
				info.size = v.second / 2;
				baseList[idOther].selfTroops.push_back(info);
			  }
			}
		  }
		  const int dist2 = baseList[idOther].nearestSelfBaseIndices[j].first;
		  const int time = dist2ToTime(dist2);
		  break;
		}
	  }
	}
  }
}


void AbstractWars::nearestMove()
{
  // それで選択されずに，飽和しそうなら，targetされていない一番近い敵の陣地に飛ばす
  for(auto i : selfBaseId)
  {
	if (baseList[i].troopsSize >= 988 && 
		baseList[i].strategyId == NOT_RESERVED)
	{
	  // int idOther = getNearestOpponentBase(i);
	  int idOther = getCornerOpponentBase(i);
	  if(idOther == -1) idOther = getNearestOpponentBase(i);
	  baseList[i].targetBaseId = idOther;
	  baseList[i].turnToAct = turn;
	  baseList[i].strategyId = NEAREST_MOVE_RESERVED;

	  TroopsInfo info = createTroops(i, idOther);
	  baseList[idOther].selfTroops.push_back(info);
	}
  }
}

void AbstractWars::revertMove()
{
  for(int other : otherBaseId)
  {
	// if(baseList[other].opponentTroops.size() == 1 &&
  }
}

// 敵同士で衝突してくれるなら，逃げる
void AbstractWars::runAwayMove()
{
  for(int selfId : selfBaseId)
  {
	set<int> playerIds;
	for(auto &t : baseList[selfId].opponentTroops)
	{
	  playerIds.insert(t.playerId);
	}

	int playerId, size;
	tie(playerId, size) = simulate(selfId);
	
	if(playerIds.size() >= 2)
	{
	  // int nearSelfId, dist2;
	  // tie(nearSelfId, dist2) = getNearSelfBase(selfId, 2, 900, allBaseId);
	  // if(nearSelfId != -1)
	  {
		int nearSelfId = getCornerOpponentBase(selfId);
		if(nearSelfId == -1) nearSelfId = getNearestOpponentBase(selfId);
		baseList[selfId].targetBaseId = nearSelfId;
		baseList[selfId].turnToAct = turn;
		baseList[selfId].strategyId = NEAREST_MOVE_RESERVED;

		TroopsInfo info = createTroops(selfId, nearSelfId);
		baseList[nearSelfId].selfTroops.push_back(info);
	  }
	}
  }
}

void AbstractWars::defenseMove()
{
  for(int selfId : selfBaseId)
  {
	int playerId, size;
	tie(playerId, size) = simulate(selfId);
	if(debug) cerr << " simulated result" << endl;
	if(debug) cerr << "(id, size) = (" << playerId << ", " << size << ")" << endl;
	if(debug) cerr << "base: " << endl;
	if(debug) cerr << baseList[selfId] << endl;
	
	if(playerId != 0)
	{
	  vector<int> vs;
	  gatherOpponentTimePrev(selfId, vs, baseList[selfId].opponentTroops);

	  vector<int> additionalSelfBase;
	  for(int aroundIndex = 0; aroundIndex < baseList[selfId].nearestSelfBaseIndices.size(); aroundIndex++)
	  {
		if(baseList[selfId].nearestSelfBaseIndices[aroundIndex].first > UBDist2) break;
		
		const int aroundSelfId = baseList[selfId].nearestSelfBaseIndices[aroundIndex].second;
		if(baseList[aroundSelfId].strategyId == NOT_RESERVED)
		{
		  additionalSelfBase.push_back(aroundSelfId);
		  
		  int resultPlayerId, aroundSize, aroundEndTime;
		  tie(resultPlayerId, aroundSize, aroundEndTime) = optimizeArrivedTurn(selfId,
																			   baseList[selfId].selfTroops,
																			   baseList[selfId].opponentTroops,
																			   vs,
																			   additionalSelfBase);
		  
		  if(resultPlayerId == 0)
		  {
			if(debug) cerr << "help! turn: " << turn << " aroundSize = " << aroundSize << " endTime = " << aroundEndTime << endl;
			if(debug) cerr << "base id: " << selfId << endl;
			if(debug) cerr << "base: " << endl;
			if(debug) cerr << baseList[selfId] << endl;

			if(debug) cerr << "self added new" << endl;
			for(int i = 0; i <= aroundIndex; i++)
			{
			  const int id = baseList[selfId].nearestSelfBaseIndices[i].second;

			  if(baseList[id].strategyId == NOT_RESERVED)
			  {
				auto troop = createTroops(id, selfId);
				troop.setEndTime(turn, aroundEndTime, speed);
				auto v = simulate(id);
				if(v.first == 0)
				{
				  troop.size = v.second / 2;
				  
				  baseList[id].targetBaseId = selfId;
				  baseList[id].turnToAct = troop.startTime;
				  baseList[id].strategyId = DEFENSE_MOVE_RESERVED;
				  baseList[selfId].selfTroops.push_back(troop);
				  if(debug) cerr << troop << endl;
				}
			  }
			}
			if(debug) cerr << "changed base: " << endl;
			if(debug) cerr << baseList[selfId] << endl;
			if(debug) cerr << "simulated result: " << endl;
			if(debug) {
			  auto vv = simulate(selfId);
			  cerr << vv.first << " " << vv.second << endl;
			}
			  
			if(debug) cerr << "--------------------" << endl;
			return;
		  }
		}
	  }
	}
  }
}

void AbstractWars::decideTroops()
{
  if(turn <= 100)
  {
	killMove();
  }

  if(playerList.size() >= 3 && speed >= 2)
  {
  	runAwayMove();
  }
  
  if(playerList.size() >= 3)
  {
	revertMove();
  }

  nearestMove();
}

vector<int> AbstractWars::sendTroops(const vector <int> &bases, const vector <int> &troops)
{
  initializeInTurn(bases, troops);
  
  if (otherBaseId.empty())
  {
	return vector<int>(0);
  }
  else if(turn == 0)
  {
	for(int i = 0; i < B; i++)
	{
	  baseSize_tmp[i] = baseList[i].troopsSize;
	}
	return vector<int>(0);
  }
  else if(turn == 1)
  {
	for(int i = 0; i < B; i++)
	{
	  baseList[i].growthRate = baseList[i].troopsSize - baseSize_tmp[i];
	}
  }

  decideTroops();

  vector<int> att;
  generateAttr(att);
  return att;
}

void AbstractWars::generateAttr(vector<int> &att)
{
  if(debug) cerr << "* SEND *" << endl;
  for(auto id : selfBaseId)
  {
	if(debug) cerr << "(playerId, turnToAct) = (" << baseList[id].playerId << ", " << baseList[id].turnToAct << ")" << endl;
	if(baseList[id].playerId == 0 && baseList[id].turnToAct == turn)
	{
	  if(debug) cerr << id << " ";
	  att.push_back(id);
	  const int idOther = baseList[id].targetBaseId;
	  att.push_back(idOther);
	}
  }
  if(debug) cerr << endl;
}

// --------------------------------------------------------------------------------
// estimation of opponent troop's arrival

void AbstractWars::removeArrivedTroops()
{
  // List1の各要素に対して，既に到達しているはずのtroopsを見つけて，除去
  for(auto &base : baseList)
  {
	auto &slf = base.selfTroops;
	for(auto it = slf.begin(); it != slf.end();)
	{
	  auto &s = baseList[it->fromBase];
	  auto &e = baseList[it->toBase];
	  it->updateCoords(turn, speed, s.y, e.y, s.x, e.x);
	  if(it->arrived(turn, speed))
	  {
		it = slf.erase(it);
	  }
	  else if(it != slf.end())
	  {
		++it;
	  }
	}
	auto &oppo = base.opponentTroops;
	for(auto it = oppo.begin(); it != oppo.end();)
	{
	  auto &s = baseList[it->fromBase];
	  auto &e = baseList[it->toBase];
	  
	  it->updateCoords(turn, speed, s.y, e.y, s.x, e.x);
	  
	  if(it->arrived(turn, speed))
	  {
		it = oppo.erase(it);
	  }
	  else if(it != oppo.end())
	  {
		++it;
	  }
	}
  }
}

void AbstractWars::removeEstimatedTroops(vector<TroopsInfo> &troops)
{
  // troopsの中でList1とマッチングが取れるものを探して，削除
  for(auto &base : baseList)
  {
	for(auto &t : base.selfTroops)
	{
	  for(int i = 0; i < troops.size(); i++)
	  {
		if(troops[i] == t)
		{
		  troops.erase(troops.begin() + i);
		  break;
		}
	  }
	}
	for(auto &t : base.opponentTroops)
	{
	  for(int i = 0; i < troops.size(); i++)
	  {
		if(troops[i] == t)
		{
		  troops.erase(troops.begin() + i);
		  break;
		}
	  }
	}
  }
}

bool AbstractWars::isValid2(const BaseInfo &baseInfo,
						   const TroopsInfo &troopInfo,
						   const Base &base) const
{
  const auto &bf = baseList[baseInfo.baseId];
  const int dy = base.y - bf.y;
  const int dx = base.x - bf.x;
  const double d = sqrt(dx * dx + dy * dy);
  const int arrivedTime = baseInfo.startTime + (int)(ceil(d / speed));
  const double rate = (turn - baseInfo.startTime) * 1.0 / (arrivedTime - baseInfo.startTime);
  const int newY = (int)(bf.y + dy * rate);
  const int newX = (int)(bf.x + dx * rate);

  return newY == troopInfo.y && newX == troopInfo.x;
  // TODO: 直前の座標とのvalidも確認(必須ではない)
}

bool AbstractWars::isValid1(const Base &base,
						   const TroopsInfo &troop) const
{
  // [from, to]が決まってない距離の枝刈りは，きちんと考える必要がある．
  const int baseY = base.y;
  const int baseX = base.x;
  const double distanceLB = distanceWithDiff(baseY - troop.y, baseX - troop.x, -0.5);
  
  return base.troopsSize / 2 == troop.size &&
	base.playerId == troop.playerId &&
	distanceLB <= speed;
}

bool AbstractWars::isValid(const BaseInfo &base,
						   const TroopsInfo &troop) const
{
  const int baseY = baseList[base.baseId].y;
  const int baseX = baseList[base.baseId].x;
  const double distanceLB = distanceWithDiff(baseY - troop.y, baseX - troop.x, -0.5);
  const double distance = speed * (turn - base.startTime);

  return base.size / 2 == troop.size &&
	base.playerId == troop.playerId &&
	distanceLB <= distance;
}

int AbstractWars::matchBaseTroop(const BaseInfo &base, const TroopsInfo &troop) const
{
  if(isValid(base, troop))
  {
	vector<int> vs;
	for(int i = 0; i < B; i++)
	{
	  if(isValid2(base, troop, baseList[i]))
	  {
		vs.push_back(i);
	  }
	}
	if(vs.size() == 1)
	{
	  return vs[0];
	}
  }
  return NONE;
}

int count_all = 0;
int count_valid12 = 0;

void AbstractWars::matchingFirstTurnTroops(vector<TroopsInfo> &troops)
{
  for(auto itTroop = troops.begin(); itTroop != troops.end(); ++itTroop)
  {
	for(int fromBase = 0; fromBase < B; fromBase++)
	{
	  // fromしか決まっていない状態での枝刈り
	  if(isValid1(baseList[fromBase], *itTroop))
	  {
		BaseInfo baseinfo;
		baseinfo.baseId = fromBase;
		baseinfo.size = baseList[fromBase].troopsSize;
		baseinfo.startTime = turn - 1;
		baseinfo.playerId = baseList[fromBase].playerId;
		
		vector<int> vs;
		for(int toBase = 0; toBase < B; toBase++)
		{
		  if(isValid2(baseinfo, *itTroop, baseList[toBase]))
		  {
			vs.push_back(toBase);
		  }
		}
		if(vs.size() == 1)
		{
		  TroopsInfo t = *itTroop;
		  t.fromBase = fromBase;
		  t.toBase = vs[0];
		  t.startTime = turn - 1;
		  t.distance = sqrt(distance2(t.fromBase, t.toBase));
		  t.arrivedTime = t.startTime + (int)(ceil(t.distance / speed));

		  if(t.playerId == 0)
		  {
			baseList[t.toBase].selfTroops.push_back(t);
		  }
		  else
		  {
			baseList[t.toBase].opponentTroops.push_back(t);
		  }
		}
		else
		{
		  nondecidedBaseInfo.push_back(baseinfo);
		}
	  }
	}
  }
}

void AbstractWars::decideNondeterminedBase(vector<TroopsInfo> &troops)
{
  // baseToがnon-decidedなbaseInfoに対して，troopsへマッチングを取る．
  // 正しければ，今度はtoに対してマッチングを取って，validが1通りに定まるなら確定
  for(auto itBase = nondecidedBaseInfo.begin(); itBase != nondecidedBaseInfo.end();)
  {
	bool find = false;
	for(auto itTroop = troops.begin(); itTroop != troops.end(); ++itTroop)
	{
	  const int toBase = matchBaseTroop(*itBase, *itTroop);
	  if(toBase != NONE)
	  {
		auto troop = *itTroop;
		troop.fromBase = itBase->baseId;
		troop.toBase = toBase;
		troop.startTime = itBase->startTime;
		troop.distance = sqrt(distance2(troop.fromBase, troop.toBase));
		troop.arrivedTime = troop.startTime + (int)(ceil(troop.distance / speed));
		// 自分で派遣した兵は，自分で登録すればよいので，敵だけでよい．

		if(troop.playerId == 0)
		{
		  baseList[troop.toBase].selfTroops.push_back(troop);
		}
		else
		{
		  baseList[troop.toBase].opponentTroops.push_back(troop);
		}

		find = true;
		itBase = nondecidedBaseInfo.erase(itBase);
		itTroop = troops.erase(itTroop);
		break;
	  }
	}
	if(!find)
	{
	  itBase++;
	}
  }
}

void AbstractWars::estimateTroops(vector<TroopsInfo> &troops)
{
  removeArrivedTroops();
  
  removeEstimatedTroops(troops);
  
  decideNondeterminedBase(troops);
  
  matchingFirstTurnTroops(troops);
}

void AbstractWars::initializeBase(const vector<BaseInfo> &bases)
{
  otherBaseId.clear();
  selfBaseId.clear();

  for (int i = 0; i < B; ++i)
  {
	baseList[i].playerId = bases[i].playerId;
	baseList[i].troopsSize = bases[i].size;

	if(baseList[i].turnToAct < turn)
	{
	  baseList[i].turnToAct = -1;
	  baseList[i].strategyId = NOT_RESERVED;
	  baseList[i].targetBaseId = -1;
	}
	
	baseList[i].nearestSelfBaseIndices.clear();

	if (baseList[i].playerId != 0)
	{
	  otherBaseId.push_back(i);
	}
	else
	{
	  selfBaseId.push_back(i);
	}
  }

  if(!selfBaseId.empty())
  {
	for(int id = 0; id < B; id++)
	{
	  for(int idSelf : selfBaseId)
	  {
		if(id != idSelf)
		{
		  const int dist2 = distance2(id, idSelf);
		  baseList[id].nearestSelfBaseIndices.push_back(make_pair(dist2, idSelf));
		}
	  }
	  auto &v = baseList[id].nearestSelfBaseIndices;
	  sort(v.begin(), v.end());
	}
  }
}

void AbstractWars::collectTroopsNum(vector<TroopsInfo> &troops, vector<BaseInfo> &bases)
{
  for(auto &p : playerList)
  {
	p.troopsNum = 0;
  }

  for(auto &t : troops)
  {
	playerList[t.playerId].troopsNum += t.size;
  }

  for(auto &b : bases)
  {
	playerList[b.playerId].troopsNum += b.size;
  }

  int maxTroopsSize = -1;
  for(int i = 1; i < playerList.size(); i++)
  {
	if(maxTroopsSize <= playerList[i].troopsNum)
	{
	  maxTroopsSize = playerList[i].troopsNum;
	  strongestPlayerId = i;
	}
  }  
}

void AbstractWars::initializeInTurn(const vector<int> &bases, const vector<int> &troops)
{
  turn++;
  int NOpp = 0;
  
  vector<BaseInfo> baseInfoList;
  for(int i = 0; i < B; i++)
  {
	BaseInfo b;
	b.baseId = 0;
	b.playerId = bases[2*i];
	b.size = bases[2*i+1];
	baseInfoList.push_back(b);
	
	NOpp = max(NOpp, b.playerId + 1);
  }
  if(turn == FIRST_TURN)
  {
	playerList.resize(NOpp);
	playerList[0].strengthUB = 1.0;
  }

  vector<TroopsInfo> troopsInfoList;
  assert(troops.size() % 4 == 0);
  const int troopsNum = troops.size() / 4;
  for(int i = 0; i < troopsNum; i++)
  {
	TroopsInfo t;
	t.playerId = troops[4*i];
	t.size = troops[4*i+1];
	t.x = troops[4*i+2];
	t.y = troops[4*i+3];
	troopsInfoList.push_back(t);
  }

  collectTroopsNum(troopsInfoList, baseInfoList);
  estimateTroops(troopsInfoList);
  initializeBase(baseInfoList);
}

// -------8<------- end of solution submitted to the website -------8<-------
#ifdef TEST
template<class T> void getVector(vector<T>& v)
{
  for (int i = 0; i < v.size(); ++i)
	cin >> v[i];
}

int main()
{
  AbstractWars aw;
  int N;
  cin >> N;
  vector<int> baseLoc(N);
  getVector(baseLoc);
  int S;
  cin >> S;
    
  int retInit = aw.init(baseLoc, S);
  cout << retInit << endl;
  cout.flush();
    
  for (int st = 0; st < 2000; ++st)
  {
	int B;
	cin >> B;
	vector<int> bases(B);
	getVector(bases);
	int T;
	cin >> T;
	vector<int> troops(T);
	getVector(troops);
        
	vector<int> ret = aw.sendTroops(bases, troops);
	cout << ret.size() << endl;
	for (int i = 0; i < (int)ret.size(); ++i)
	{
	  cout << ret[i] << endl;
	}
	cout.flush();
  }
  return 0;
}
#endif // TEST
