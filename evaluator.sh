#!/bin/bash

g++ -std=c++11 main.cpp

size=1000

parallel --progress --results results 'java AbstractWarsVis -exec "./a.out" -seed {} -novis 2> /dev/null' ::: `seq 1 $size`

python calc_score.py $size
